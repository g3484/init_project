echo "Running git clone frontend ..."
git clone https://gitlab.com/g3484/next-goodfood-frontend.git client
cd client 
git checkout -b develop
npm install
cd ..

echo "Running git clone compta ..."
git clone https://gitlab.com/g3484/next-goodfood-compta.git compta
cd compta 
git checkout -b develop
npm install
cd ..

echo "Running git clone admin ..."
git clone https://gitlab.com/g3484/next-goodfood-admin.git admin
cd admin 
git checkout -b develop
npm install
cd ..

echo "Running git clone infra ..."
git clone https://gitlab.com/g3484/infra.git
cd infra
git checkout -b develop
cd ..

echo "Running git clone orders ..."
git clone https://gitlab.com/g3484/node-microservice-order.git orders
cd orders
git checkout -b develop
npm install
cd ..

echo "Running git clone products ..."
git clone https://gitlab.com/g3484/node-microservice-product.git products
cd products
git checkout -b develop
npm install
cd ..

echo "Running git clone payments ..."
git clone https://gitlab.com/g3484/node-microservice-payments.git payments
cd payments
git checkout -b develop
npm install
cd ..

echo "Running git clone deliveries ..."
git clone https://gitlab.com/g3484/node-goodfood-delivery.git deliveries
cd deliveries
git checkout -b develop
npm install
cd ..

echo "Running git clone users ..."
git clone https://gitlab.com/g3484/csharp-goodfood-users.git users
cd users
git checkout -b develop
cd ..

echo "Running git clone statistics ..."
git clone https://gitlab.com/g3484/golang-goodfood-statistics.git statistics
cd statistics
git checkout -b develop
cd ..

echo "Running git clone common ..."
git clone https://gitlab.com/g3484/node-goodfood-common.git common
cd common
git checkout -b develop
npm install
cd ..

git checkout -b develop

echo "Done !"
